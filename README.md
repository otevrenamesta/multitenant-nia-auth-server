# multitenant-nia-auth-server

## routes

- /{{TENANTID}}/login: redirect user to NIA login
- /{{TENANTID}}/login_assert: is called by NIA on successful login. 
It stringify the incomming data incl. user profile (in JSON) to url param __res__,
and redirects to url in __tenantconfig__.login_redirect_url.
- /{{TENANTID}}/logout: sessionindex and nameid are required query params.
These are available in login response, you have to store them.
User is redirected to NIA for logout.
- /{{TENANTID}}/logout_assert: the is call by NIA after logout.
Data again packet into __res__ url param and redir to __tenantconfig__.logout_redirect_url.

## config

Via env vars:

- HOST: host to bind to (default 127.0.0.1)
- PORT: port (default 3000)
- CONFIG_FOLDER: folder where config files of particular tenants live. 
Naming convention see below.
- AUDIENCE_URL: full url to where SAML responses will be sent. 
Contains {{TENANTID}} replace pattern, that will be replaces with given tenant id from request. 
E.g. https://04cc-178-255-168-237.eu.ngrok.io/{{TENANTID}}/login_assert
- SECRET: secret phrase for partner comm

## tenant configs

One tenant needs 3 config files:
- public cert file: {{TENANTID}}.crt
- private key file: {{TENANTID}}.key
- rest config (__tenantconfig__) in YAML: {{TENANTID}}.yaml

Otherwise requests will fail.

crt and key file can be generated like this:

```
TENANTID=testtenant
openssl req -x509 -nodes -days 1000 -newkey rsa:2048 -keyout $TENANTID.key -out $TENANTID.crt
```

__tenantconfig__ can also contain following:

- debug: boolean saying if testing NIA should be used.