import {APIError} from './errors'
import app from './index'

const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

app.use((error, req, res, next) => {
  if (error instanceof APIError) {
    return res.status(error.name).send(error.message)
  }
  next(error)
})

app.use((error, req, res, next) => {
  process.env.LOG_500_ERRORS && console.error(error)
  res.status(500).send(error.message || error.toString())
})

app.listen(port, host, (err) => {
  if (err) throw err
  console.log(`gandalf whispers on ${host}:${port}`)
})
