import fs from 'fs'
import path from 'path'
import assert from 'assert'
import yaml from 'yaml'
import {APIError} from './errors'

assert.ok(process.env.CONFIG_FOLDER, 'env.CONFIG_FOLDER not defined!')
assert.ok(process.env.AUDIENCE_URL, 'env.AUDIENCE_URL not defined!')

const CONFIG_FOLDER = process.env.CONFIG_FOLDER
const AUDIENCE_URL = process.env.AUDIENCE_URL

export async function loadTenantConfig (tenantid) {
  try {
    const filepath = path.join(CONFIG_FOLDER, `${tenantid}.yaml`)
    const src = await fs.promises.readFile(filepath, 'utf8')
    const cfg = yaml.parse(src)
    
    cfg.audience = AUDIENCE_URL.replace('{{TENANTID}}', tenantid)

    const certfile = path.join(CONFIG_FOLDER, `${tenantid}.crt`)
    cfg.certificate = await fs.promises.readFile(certfile, 'utf8')

    const keyfile = path.join(CONFIG_FOLDER, `${tenantid}.key`)
    cfg.private_key = await fs.promises.readFile(keyfile, 'utf8')
    
    cfg.id = tenantid
    return cfg
  } catch (err) {
    throw new APIError(400, 'unknown tenant or wrong config')
  }
}
