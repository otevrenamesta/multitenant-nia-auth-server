import express from 'express'
import NIA from 'node-nia-connector'
import api from './routes'
import { loadTenantConfig } from './configman'

const app = express()

app.use('/:tenantid', (req, res, next) => {
  loadTenantConfig(req.params.tenantid)
    .then(cfg => {
      req.NIAConnector = new NIA({
        audience: cfg.audience,
        private_key: cfg.private_key,
        certificate: cfg.certificate,
        assert_endpoint: cfg.audience
      }, cfg.debug)
      req.tenantcfg = cfg
      next()
    })
    .catch(next)
}, api)

export default app