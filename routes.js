import NIA from 'node-nia-connector'
import express from 'express'
import jwt from 'jsonwebtoken'
import _ from 'underscore'

const api = express()
const bodyParser = express.urlencoded({ extended: true })
const SECRET = process.env.SECRET

api.get('/login', function (req, res, next) {
  const opts = {
    attrs: [
      { name: NIA.PROFILEATTRS.PERSON_IDENTIFIER, required: true },
      { name: NIA.PROFILEATTRS.GIVEN_NAME, required: true },
      { name: NIA.PROFILEATTRS.FAMILY_NAME, required: false },
      { name: NIA.PROFILEATTRS.EMAIL, required: true },
      { name: NIA.PROFILEATTRS.CZMORIS_PHONE_NUMBER, required: false },
    ],
    level: NIA.LOA.SUBSTANTIAL
  }

  req.NIAConnector.createAuthRequestUrl(opts).then(loginUrl => {
    res.redirect(loginUrl)
  }).catch(next)
})

api.post('/login_assert', bodyParser, async function (req, res, next) {
  const { login_redirect_url } = req.tenantcfg
  try {
    const samlResponse = await req.NIAConnector.postAssert(req.body)
    const data = _.extend(_.omit(samlResponse, 'header', 'LoA'), { tenant: req.tenantcfg.id })
    const token = jwt.sign(data, SECRET, { expiresIn: 60 })
    res.redirect(`${login_redirect_url}?t=${token}`)
  } catch(err) {
    next(err)
  }
})

api.get('/logout', (req, res, next) => {
  if (!req.query.nameid) return next('query.nameid missing')
  if (!req.query.sessionindex) return next('query.sessionindex missing')
  const { sessionindex, nameid } = req.query
  req.NIAConnector.createLogoutRequestUrl(nameid, sessionindex)
    .then(logoutUrl => {
      res.redirect(logoutUrl)
    })
    .catch(next)
})

api.post('/logout_assert', bodyParser, (req, res, next) => {
  const { logout_redirect_url } = req.tenantcfg
  try {
    const samlResponse = req.NIAConnector.logoutAssert(req.body)
    const data = _.extend(samlResponse, { tenant: req.tenantcfg.id })
    res.redirect(`${logout_redirect_url}?res=${JSON.stringify(data)}`)
  } catch (err) {
    next(err)
  }
})

export default api
